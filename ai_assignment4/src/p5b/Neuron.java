package p5b;

import java.util.List;
import java.util.Random;

public class Neuron {

	
	private double[][] inputData;
	private int[] outputs;
	private List<Neuron> predLayer;
	private Random rand;
	
	public Neuron(double[][] inputData, int[] outputs, List<Neuron> predLayer, Random rand) {
		this.inputData = inputData;
		this.outputs = outputs;
		this.predLayer = predLayer;
		this.rand = rand;
	}
	
	

}
