package p5b;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MLP {

	private double[][] inputData;
	
	private int[] outputs;
	
	private int inputDataCount;
	
	private int inputDimension;
	
	private List<Neuron> hiddenLayer;
	
	private List<Neuron> outputLayer;
	
	public MLP(double[][] inputData, int[] outputs,int num_input,int num_hidden,int num_output) {
		this.inputData = inputData;
		this.outputs = outputs;
		inputDataCount = inputData.length;
		inputDimension = inputData[0].length;
		
		Random rand = new Random();
		
		
		hiddenLayer = new ArrayList<>(num_hidden);
		outputLayer = new ArrayList<>(num_output);
		
		
		for(int i = 0; i < num_hidden; i++){
			Neuron n = new Neuron(inputData,null,outputLayer,rand);
			hiddenLayer.add(n);
		}
		for(int i = 0; i < num_output; i++){
			Neuron n = new Neuron(null,outputs,hiddenLayer,rand);
			outputLayer.add(n);
		}
		
		
		
		
	}

	public void startTraining(int max_iterations) {
		// TODO Auto-generated method stub
		
	}

}
