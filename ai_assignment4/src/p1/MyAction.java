package p1;

public class MyAction {

	public final int actionId;//not unique
	public final double probability;
	public final MyState to;
	
	public MyAction(int actionId, double probability, MyState to){
		this.actionId = actionId;
		this.probability = probability;
		this.to = to;
	}
}
