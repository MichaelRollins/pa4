package p1;

import java.util.ArrayList;
import java.util.List;

public class MDPBuilder {

	private List<MyState> mdpList;
	
	public MDPBuilder(int numberStates){
		mdpList = new ArrayList<>(numberStates);
	}
	
	public void createState(int stateId,double reward,boolean exitState){//general assumption ids are unique
		MyState state = new MyState(stateId,reward,exitState);
		mdpList.add(state);
	}
	
	public void addTranstion(int fromId, int actionId, int toId, double actionProbability){
		MyState from = null;
		MyState to = null;
		
		for(int i = 0; i < mdpList.size(); i++){
			int test = mdpList.get(i).getStateId();
			
			if(test == fromId){
				from = mdpList.get(i);
			}
			
			if(test == toId){
				to = mdpList.get(i);
			}
			
			if(from != null && to != null){
				break;
			}
		
		}
		
		if(from != null && to != null){
			from.addTransition(to,actionId,actionProbability);
		} else {
			System.out.println("One or more ids do not exist!");
		}
		
	}
	
	public List<MyState> getMDPList(){
		return mdpList;
	}

	
	
}
