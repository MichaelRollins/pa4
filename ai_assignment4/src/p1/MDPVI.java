package p1;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class MDPVI {

	private List<MyState> mdpList;
	
	private int iteration;
	
	private double gamma;
	
	private double error;

	private double reward;
	
	public MDPVI(List<MyState> mdpList,double reward , double discount, double error) {
		this.mdpList = mdpList;
		gamma = discount;
		this.error = error;
		iteration = 0;
		this.reward = reward;
	}
	
	public void start(){
		System.out.println("Iteration: " + iteration );
		printStateUtility();
		printOptimalPolicy();
		
		while(true){	
			double delta = 0;
		
			
			for(MyState nextState: mdpList){
				
				if(!nextState.isExitState()){
					List<MyAction> trans = nextState.getTransitions();
					
					
					
					
					Set<Integer> uniqueActions = new LinkedHashSet<>();
					for(MyAction t : trans){
						uniqueActions.add(t.actionId);
					}
					
					double maxTrans = 0; 
					MyState route = null;
					for(int aId :uniqueActions){//summation step
						double sumTrans = 0;
						for(MyAction nextTrans: trans){
							if(nextTrans.actionId == aId){
								sumTrans += nextTrans.probability * nextTrans.to.getUtility();
								if(sumTrans > maxTrans){
									maxTrans = sumTrans;
									route = nextTrans.to;
								}
							}	
						}
					}
					
					
					double utility = reward + (gamma * maxTrans);
					if(utility - nextState.getUtility() > delta){
						delta = utility - nextState.getUtility();
					}
					nextState.setUtility(utility);
					nextState.setOptimalPath(route);
					//refer to page 693 of book to finish
				
					
					//System.out.println("Delta: " + delta);
					//System.out.println("Other: "+ ((error*(1-gamma))/gamma));
				}
				
				
				
			}
			
			if(delta < ((error*(1-gamma))/gamma)){//continue until...
				break;
			}
			
			
			iteration++;
			
			/*
			if(iteration == stopValue){
				System.out.println("Iteration: " + iteration );
				printStateUtility();
				break;
			} else {
				System.out.println("Iteration: " + iteration );
				printStateUtility();
			}
			*/
		
			
			System.out.println("Iteration: " + iteration );
			printStateUtility();
			printOptimalPolicy();
			
		}
		
		
	}
	
	private void printOptimalPolicy() {
		System.out.println("Optimal Policy:");
		for(MyState state: mdpList){
			if(state.getOptimalPolicy() < 0){
				System.out.println("State Id: s"+ state.getStateId() + " has no policy");
			} else {
				System.out.println("State Id: s"+ state.getStateId() + " with Policy: a" + state.getOptimalPolicy());
			}
		}
		System.out.println();
		
	} 

	public void printStateUtility(){
		System.out.println("State Utilities:");
		for(MyState state: mdpList){
			System.out.println("State Id: s"+ state.getStateId() + " with Utility: " + state.getUtility());
		}
	}

	public int getNumberOfIteration() {
		return iteration;
	}

}
