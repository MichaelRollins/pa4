package p1;

import java.util.ArrayList;
import java.util.List;

public class MyState {

	private int stateId;//this states id

	private List<MyAction> transitions;//possible transitions
	
	//private double reward;
	
	private double utility;
	
	private MyState optimalPath;
	
	private boolean exitState;
	
	public MyState(int stateId, double reward, boolean exitState){
		this.stateId = stateId;	
		transitions = new ArrayList<>();
		//this.reward = reward;
		utility = reward;
		optimalPath = null;
		this.exitState = exitState;
	}
	
	

	public int getStateId(){
		return stateId;
	}

	//actionIds maybe non-unique
	public void addTransition(MyState to, int actionId, double actionProbability) {
		transitions.add(new MyAction(actionId,actionProbability,to));
	}

	public double getUtility() {
		return utility;
	}
	
	public List<MyAction> getTransitions(){
		return transitions;
	}
	
	public void setUtility(double utility){
		this.utility = utility;
	}
	
	public boolean isExitState(){
		return exitState;
	}
	
	public int getOptimalPolicy(){
		for(MyAction action:transitions){
			if(action.to.equals(optimalPath)){
				return action.actionId;
			}
		}
		return -1;
	}

	/*
	public MyState getOptimalPath(){
		return optimalPath;
	}
	*/

	public void setOptimalPath(MyState route) {
		optimalPath = route;
	}
	
}
