package p1;

import java.util.List;

public class Main {

	
	public static void main(String[] args){
		
		
		MDPBuilder mdpBuild = new MDPBuilder(4);
		mdpBuild.createState(1,0,false);//stateId / Reward /
		mdpBuild.createState(2,0,false);
		mdpBuild.createState(3,1,true);
		mdpBuild.createState(4,0,false);
		
		
		mdpBuild.addTranstion(1, 1, 1, 0.2); //fromState, actionId, toState,probability of action
		mdpBuild.addTranstion(1, 1, 2, 0.8);
		mdpBuild.addTranstion(1, 2, 1, 0.2);
		mdpBuild.addTranstion(1, 2, 4, 0.8);
		mdpBuild.addTranstion(2, 2, 2, 0.2);
		mdpBuild.addTranstion(2, 2, 3, 0.8);
		mdpBuild.addTranstion(2, 3, 2, 0.2);
		mdpBuild.addTranstion(2, 3, 1, 0.8);
		mdpBuild.addTranstion(3, 4, 2, 1);
		mdpBuild.addTranstion(3, 3, 4, 1);
		mdpBuild.addTranstion(4, 1, 4, 0.1);
		mdpBuild.addTranstion(4, 1, 3, 0.9);
		mdpBuild.addTranstion(4, 4, 4, 0.2);
		mdpBuild.addTranstion(4, 4, 1, 0.8);
		
		
		List<MyState> mdpList = mdpBuild.getMDPList();
		
		MDPVI mdpvi = new MDPVI(mdpList,-0.0003,0.90,0.05);
		//mdpvi.start(10);
		long startTime = System.currentTimeMillis();
		mdpvi.start();
		long endTime   = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		System.out.println("Computation time: " + totalTime+"ms");
		
		System.out.println("Number of Iterations: " + mdpvi.getNumberOfIteration());
		
	}
}
