package p4c;

public class Perceptron {

	int max_iterations;
	private int number_points;
	
	private double[] x;
	
	private double[] y;
	
	private int[] outputs;
	private double[] weights;
	
	private double lr = 0.1;
	
	private double minimumError;
	private double[] bestWeights;
	
	public Perceptron(int number_points, double[] x, double[] y, int[] outputs, double[] weights) {
		this.number_points = number_points;
		this.x = x;
		this.y = y;
		this.outputs = outputs;
		this.weights = weights;	
		this.bestWeights = new double[weights.length];
	}

	public void startTraining(int max_iterations) {
		this.max_iterations = max_iterations;
		
		double locErr, globErr;
		
		int iter = 0;
		
		minimumError = number_points;//worst case
		do{
			iter++;
			globErr = 0;
			
			for(int i = 0; i < number_points; i++){
				
				int classification = outputClass(weights,x[i]);
				
				locErr = outputs[i] - classification;
				
				weights[0] += lr * locErr;
				weights[1] += lr * locErr * x[i];
				//weights[2] += lr * localError * y[i];
				
				globErr += (locErr * locErr);
				
			}
			
			if(globErr < minimumError){
				minimumError = globErr;
				bestWeights[0] = weights[0];
				bestWeights[1] = weights[1];
				//bestWeights[2] = weights[2];
			}
			
			//System.out.println("Standard form: " + weights[1] +"x + y = " + (-weights[0]));
			//System.out.println("y = "+ (-weights[1]) + "x + " + (-weights[0]));
			
		} while(globErr != 0 && iter <= max_iterations);
		System.out.println("Standard form: " + bestWeights[1] +"x + y = " + (-bestWeights[0]));
		System.out.println("Decision equation: y = mx + b");
		System.out.println("y = "+ (-bestWeights[1])+ "x + " + (-bestWeights[0]));
		System.out.println("Classification after iteration: " + iter);
		System.out.println("Minimum error: " + minimumError);
		
		
		
	}

	private int outputClass(double[] w, double x) {
		
		//sum = bias + x * w[1] + y * w[2]
		double sum = w[0] + (x * w[1]);
		if(sum >= 0){
			return 1;
		} else {
			return 0;
		}
	}

	
}
