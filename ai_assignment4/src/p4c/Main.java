package p4c;



public class Main {

		public static void main(String[] args){
			//Random rand = new Random();
			
			int max_iterations = 100;
			int number_points = 8;
			double[] x = {0.1, 0.13, 0.28, 0.34, 0.46, 0.6, 0.73, 0.93};
			double[] y = {0.72, 1.02, 0.58, 0.98, 0.16, 0.3, 0.66, 0.45};		
			int[] outputs = {0, 1, 0, 1, 0, 0, 1, 1};
			double[] weights = new double[2];
			
			/*
			StringBuilder sb1 = new StringBuilder();
			StringBuilder sb2 = new StringBuilder();
			for(int i  = 0; i < x.length; i++){
				if(outputs[i] == 0){
					
					sb1.append("(" +x[i] +","+ y[i] +"),");
				} else {
					sb2.append("(" +x[i] +","+ y[i] +"),");
				}
			}
			System.out.println(sb1.toString());
			System.out.println(sb2.toString());
			System.out.println();
			*/
			
			weights[0] = 0.2;
			weights[1] = 0.6;
			//weights[2] = -1;
			
			
			
					
			Perceptron p = new Perceptron(number_points,x,y,outputs,weights);
			p.startTraining(max_iterations);
			
			
		}
}
