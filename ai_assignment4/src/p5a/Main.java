package p5a;


public class Main {

		public static void main(String[] args){
			//Random rand = new Random();
			
			int max_iterations = 100000;
			int number_points = 12;
			double[] x = {0.02, 0.11, 0.12, 0.21, 0.24, 0.35, 0.38, 0.44, 0.54, 0.71, 0.79, 0.94};
			double[] y = {0.48, 0.72, 1.12, 0.48, 0.31, 0.36, 0.77, 0.48, 0.24, 0.64, 0.27, 0.44};
			int[] outputs = {1,0,1,0,0,0,1,0,1,1,1,1};
			
			/*
			StringBuilder sb1 = new StringBuilder();
			StringBuilder sb2 = new StringBuilder();
			for(int i  = 0; i < x.length; i++){
				if(outputs[i] == 0){
					
					sb1.append("(" +x[i] +","+ y[i] +"),");
				} else {
					sb2.append("(" +x[i] +","+ y[i] +"),");
				}
			}
			System.out.println(sb1.toString());
			System.out.println(sb2.toString());
			System.out.println();
			*/
			
			
			double[] weights = new double[3];
			
			weights[0] = 0.4;
			weights[1] = 0.8;
			weights[2] = 0.7;
			
			
			
					
			Perceptron p = new Perceptron(number_points,x,y,outputs,weights);
			p.startTraining(max_iterations);
			//p.getMinimumError();
			
			
		}
}
